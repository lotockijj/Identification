﻿using System.Threading.Tasks;

namespace Identification.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
